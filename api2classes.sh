#!/bin/bash

# This script used for converting an api file to a list of qualified class names
# After building AOSP, api files can be found at "/android_build/out/target/common/obj/PACKAGING/"

if [ -z "$1" ]; then
	echo "Usage: ./$0 INPUT_FILE [OUTPUT_FILE]"
	echo "Ex: ./$0 public_api.txt classes.txt"
	exit 1
fi

if [ -n "$2" ] && [ -f "$2" ]; then
	echo "Error: $2 is already existed!"
	exit 1
fi

PKGS=($(grep -oP -n "(?<=package\s).+(?=\s{)" $1))
LEN=${#PKGS[@]}

for (( i=1; i<$LEN; i++ )); do
	PKGS[$i-1]=${PKGS[$i-1]}:$(echo ${PKGS[$i]} | cut -d ':' -f 1)
done

PKGS[$LEN-1]=${PKGS[$LEN-1]}:$(cat $1 | wc -l)

for pkg in "${PKGS[@]}"; do
	read -r beg pname len <<< $(echo $pkg | awk -F ':' '{ print $1,$2,$3-$1 }')
	elements=($(tail -n +$beg $1 | head -n $len | grep -oP "(?<=\sclass\s|\sinterface\s)[^\s<]+"))
	for e in "${elements[@]}"; do
		if [ -z "$2" ]; then
			echo $pname.$e
		else
			echo $pname.$e >> $2
		fi
	done
done

test -n "$2" && echo "Done!"
