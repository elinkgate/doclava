# What is this?
This modified version of doclava provides a new option to ignore the @hide annotation when generating stubs.
# Guide to build Signee SDK:
Step 1: Patch this version to external/doclava and then build it:
```
$ make -j doclava
```
Step 2: Build the Android AOSP, then use api2classes.sh to generate a list of classes will be shown in SDK.
```
$ make -j
$ bash api2classes.sh out/target/common/obj/PACKAGING/public_api.txt signee_sdk_classes.txt
```
Step 3: Add a new target to frameworks/base/Android.mk:
```
# ====  the signee system api stubs ===================================
include $(CLEAR_VARS)

LOCAL_SRC_FILES:=$(framework_docs_LOCAL_API_CHECK_SRC_FILES)
LOCAL_GENERATED_SOURCES:=$(framework_docs_LOCAL_GENERATED_SOURCES)
LOCAL_SRCJARS:=$(framework_docs_LOCAL_SRCJARS)
LOCAL_JAVA_LIBRARIES:=$(framework_docs_LOCAL_API_CHECK_JAVA_LIBRARIES)
LOCAL_MODULE_CLASS:=$(framework_docs_LOCAL_MODULE_CLASS)
LOCAL_DROIDDOC_SOURCE_PATH:=$(framework_docs_LOCAL_DROIDDOC_SOURCE_PATH)
LOCAL_DROIDDOC_HTML_DIR:=$(framework_docs_LOCAL_DROIDDOC_HTML_DIR)
LOCAL_ADDITIONAL_JAVA_DIR:=$(framework_docs_LOCAL_API_CHECK_ADDITIONAL_JAVA_DIR)
LOCAL_ADDITIONAL_DEPENDENCIES:=$(framework_docs_LOCAL_ADDITIONAL_DEPENDENCIES)

LOCAL_MODULE := signee-system-api-stubs

LOCAL_DROIDDOC_STUB_OUT_DIR := $(TARGET_OUT_COMMON_INTERMEDIATES)/JAVA_LIBRARIES/signee_android_system_stubs_current_intermediates/src

SIGNEE_SYSTEM_API_FILE=$(TARGET_OUT_COMMON_INTERMEDIATES)/PACKAGING/signee-system-api.txt
SIGNEE_SYSTEM_REMOVED_API_FILE=$(TARGET_OUT_COMMON_INTERMEDIATES)/PACKAGING/signee-removed-api.txt
SIGNEE_SYSTEM_EXACT_API_FILE=$(TARGET_OUT_COMMON_INTERMEDIATES)/PACKAGING/signee-exact-api.txt

LOCAL_DROIDDOC_OPTIONS:=\
		$(framework_docs_LOCAL_DROIDDOC_OPTIONS) \
		-hidden \
		-show file://./signee_sdk_classes.txt \
		-show android.hardware.biometrics.BiometricAuthenticator \
		-strip 'android.graphics.Canvas.VertexMode.nativeInt' \
		-strip 'android.graphics.Canvas.EdgeType.nativeInt' \
		-strip 'android.graphics.PorterDuff.Mode.nativeInt' \
		-strip 'android.graphics.Region.Op.nativeInt' \
		-strip 'android.media.audiofx.AudioEffect.AudioEffect(UUID,UUID,int,int)' \
		-referenceonly \
		-api $(SIGNEE_SYSTEM_API_FILE) \
		-removedApi $(SIGNEE_SYSTEM_REMOVED_API_FILE) \
		-exactApi $(SIGNEE_SYSTEM_EXACT_API_FILE) \
		-nodocs

LOCAL_DROIDDOC_CUSTOM_TEMPLATE_DIR:=external/doclava/res/assets/templates-sdk

LOCAL_UNINSTALLABLE_MODULE := true

include $(BUILD_DROIDDOC)

$(full_target): .KATI_IMPLICIT_OUTPUTS := $(SIGNEE_SYSTEM_API_FILE) \
                                          $(SIGNEE_SYSTEM_REMOVED_API_FILE) \
                                          $(SIGNEE_SYSTEM_EXACT_API_FILE)
$(call dist-for-goals,sdk,$(SIGNEE_SYSTEM_API_FILE))
```
Step 4: Add a new target to development/build/Android.mk:
```
# ============ Signee System SDK ============
sdk_stub_name := signee_android_system_stubs_current
stub_timestamp := $(OUT_DOCS)/signee-system-api-stubs-timestamp
include $(LOCAL_PATH)/build_android_stubs.mk

.PHONY: signee_android_system_stubs
signee_android_system_stubs: $(full_target) $(full_src_target)

# Build and store the android_system.jar.
$(call dist-for-goals,sdk win_sdk,$(full_target):signee_android_system.jar)
```
Step 5: Build the stubs:
```
$ make signee_android_system_stubs
```
Step 6: In development/build/sdk.atree, comment out these lines:
```
# System images + Kernel
#system-qemu.img    system-images/${PLATFORM_NAME}/${TARGET_CPU_ABI}/system.img
#vendor-qemu.img    system-images/${PLATFORM_NAME}/${TARGET_CPU_ABI}/vendor.img
#ramdisk.img        system-images/${PLATFORM_NAME}/${TARGET_CPU_ABI}/ramdisk.img
#device/generic/goldfish/data/etc/userdata.img  system-images/${PLATFORM_NAME}/${TARGET_CPU_ABI}/userdata.img
#data/misc          system-images/${PLATFORM_NAME}/${TARGET_CPU_ABI}/data/misc
#system/build.prop  system-images/${PLATFORM_NAME}/${TARGET_CPU_ABI}/build.prop
```
Step 7: Build SDK:
```
make -j win_sdk
```

Find output in out/host/windows/sdk/signee_mx8mq/